- [x] REACT-01 - Bootstrap create-react-app project
- [x] REACT-02 - Setup a simple routing system that allow me to render more pages
- [x] REACT-03 - Create basic html for three pages: jobsList, jobApplication, jobFeedback
- [x] REACT-04 - Style the pages and make them look nice, including mobile breakpoints
- [x] REACT-05 - Setup react-storybook
- [x] REACT-06 - Create a job advertisement component
- [x] ~~REACT-07 - Create a department section component~~
- [x] REACT-08 - Create an apply for this job button
- [x] REACT-09 - Create a submit application button with loading
- [x] REACT-10 - Create a back button
- [x] REACT-11 - Create a job applied feedback component
- [x] REACT-12 - Create an empty state for no jobs available
- [x] REACT-13 - Create an empty state for failed requests
- [x] REACT-14 - Create an input component with error options
- [x] REACT-15 - Create an alert box for failed form fields
- [x] REACT-16 - Create a contact form
- [x] REACT-17 - Create an e-mail validation for the form
- [x] REACT-18 - Fetch jobs from backend
- [x] REACT-19 - Setup form submission
- [x] REACT-20 - Setup dynamic endpoint url
- [x] REACT-21 - Setup a pipeline to build it on every push
- [x] REACT-22 - Beautify README explaning some choices and how to develop


- [x] RAILS-01 - Bootstrap a ruby on rails project
- [x] RAILS-02 - Create a job model
- [x] RAILS-03 - Create an endpoint that fetches data from jobscore feed url
- [x] RAILS-04 - Create a job candidate model
- [x] RAILS-05 - Create an endpoint that receives application request and validate all fields
- [x] RAILS-06 - Send a slack message once application request succeeds
- [x] RAILS-07 - Setup a pipeline to build it on every push
- [x] RAILS-08 - Beautify README explaning some choices and how to develop
- [x] RAILS-09 - Send job id with candidate info to slack


- [x] OPS-01 - Setup an environment with nginx serving static content
- [X] OPS-02 - Use nginx as a reverse proxy for backend
- [x] OPS-03 - Create a Dockerfile to easyily deploy everything
- [x] OPS-04 - Create a pipeline to automatic deploy to an ec2 instance
- [x] OPS-05 - Beautify README explaning how to deploy the app with docker