## Jobs Application

### Specifications
Project specifications are described in each of its modules, be sure to check it out: 
* [Frontend](https://gitlab.com/fedevegili/jobs/tree/master/frontend)
* [Backend](https://gitlab.com/fedevegili/jobs/tree/master/backend)

### How am I going to do it? 123456

First of all, I'm going to split this assestment in a few tasks to help me focus and keep progress. The tasks are listed [here](https://gitlab.com/fedevegili/jobs/blob/master/TASKS.md).

There will be a merge request for every task so it is easy to analyze what was needed for every task. MR list can be found [here](https://gitlab.com/fedevegili/jobs/merge_requests?state=all).

### Pipelines
![pipelines](https://i.imgur.com/95Oq6l8.png)

I set-up a pipeline system to ensure quality and deploy everything necessary automatically. The process will build and test, if everything is correct it will deploy the app to a staging server.

It will also publish static content to gitlab pages. More info is available in each module README.

### Developing guide
There are specific instructions for each module here:
* [Frontend](https://gitlab.com/fedevegili/jobs/tree/master/frontend#how-to-develop)
* [Backend](https://gitlab.com/fedevegili/jobs/tree/master/backend#how-to-develop)

### Deploying to production with docker
Docker was used to keep environment consistency and easily deploy it anywhere.

A nginx server was set-up to serve static front-end content and reverse proxy the rails backend.

The preferred way of deploying is using the staging pipeline. If you would like to do it be yourself, follow these steps:
* Clone the repository
* [Download and unpack](https://gitlab.com/fedevegili/jobs/-/jobs/artifacts/master/download?job=build) compiled modules in the root of your repo
* Set the necessary [environment variables](https://gitlab.com/fedevegili/jobs/tree/RAILS-08/backend#secrets)
* `cd` into project root and run `docker-compose up`
* The system will be available at http://localhost
