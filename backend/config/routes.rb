Rails.application.routes.draw do
  scope(:path => '/api') do
    resources :jobs, :only => [:index, :show]
    resources :candidates, :only => [:create]
  end
end
