class Candidates
    include ActiveModel::Model
    attr_accessor :name, :lastName, :email, :city, :postalCode, :coverLetter, :jobId
    validates :name, :lastName, :email, :city, :postalCode, :coverLetter, :jobId, presence: true
end