class CandidatesController < ApplicationController
    rescue_from ::Slack::SlackExceptions::MsgFailedError, with: :onError

    def create
        candidate = Candidates.new(params.require(:candidate).permit(:name, :lastName, :email, :city, :postalCode, :coverLetter, :jobId))

        if !candidate.valid?
            return render json: candidate.errors, status: 500
        end

        slack = Slack::SlackApi.new()

        slack.sendCandidateNotification(candidate)

        render json: {}, status: :ok
    end

    def onError(err)
        render json: err.render, status: 500
    end
end
