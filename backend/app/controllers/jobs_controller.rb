class JobsController < ApplicationController
    rescue_from ::JobScore::JobScoreExceptions::FeedFailedError, with: :onError

    def index
        api = JobScore::JobScoreApi.new()

        jobs = api.list()

        render json: {jobs: jobs}, status: :ok
    end

    def show
        api = JobScore::JobScoreApi.new()

        foundJob = api.getJobById(params[:id])

        render json: {job: foundJob}, status: :ok
    end

    def onError(err)
        render json: err.render, status: 500
    end

end
