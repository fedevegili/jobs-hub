module JobScore
    module JobScoreExceptions
        class FeedFailedError < StandardError
            def render
                {"message" => "failed to communicate with jobScore api, try again later"}
            end
        end
    end
end