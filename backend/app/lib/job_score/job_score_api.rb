require 'httparty'

module JobScore
    class JobScoreApi
        def list
            getJobs()
        end

        def getJobById(id)
            jobs = getJobs()

            foundJob = nil

            jobs.each { |job|
                if job.id === id
                    foundJob = job;
                end
            }

            return foundJob
        end

        private

        def getJobs
            Rails.cache.fetch("jobs", expire_in: 10.minutes) do
                cachedData = fetchAll()
            end
        end

        def fetchAll
            puts "Fetching jobScore feed"

            url = 'https://careers.jobscore.com/jobs/jobscore/feed.json'

            response = HTTParty.get(url)

            if response.code != 200
                raise JobScore::JobScoreExceptions::FeedFailedError
            end

            jobs = response.parsed_response['jobs']

            jobsModels = []

            jobs.each { |job|
                jobModel = Jobs.new(
                    {
                        "title" => job['title'],
                        "id" => job['id'],
                        "location" => job['location'],
                        "department" => job['department'],
                        "description" => job['description'],
                        "job_type" => job['job_type'],
                        "postal_code" => job['postal_code']
                    }
                )

                jobsModels.push(jobModel)
            }

            return jobsModels
        end

    end
end