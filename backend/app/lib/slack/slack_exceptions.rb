module Slack
    module SlackExceptions
        class MsgFailedError < StandardError
            def render
                {"message" => "slack message send failed"}
            end
        end
    end
end