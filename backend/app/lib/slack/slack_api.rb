require 'httparty'

module Slack
    class SlackApi
        def sendCandidateNotification(candidate)
            response = HTTParty.post(
                "https://hooks.slack.com/services/#{ENV['SLACK_HOOK']}",
                :headers => {'content-type': 'application/json'},
                :body => {
                    "text" => "-----\nNew candidate!\n\n" +
                        "Job: #{candidate.jobId}\n" +
                        "Name: #{candidate.name}\n" +
                        "Lastname: #{candidate.lastName}\n" +
                        "Email: #{candidate.email}\n" +
                        "City: #{candidate.city}\n" +
                        "Postal code: #{candidate.postalCode}\n" +
                        "Cover letter: #{candidate.coverLetter}\n" +
                        "-----"
                }.to_json
            )

            if response.code != 200
                raise Slack::SlackExceptions::MsgFailedError
            end
        end
    end
end