require 'test_helper'

class CandidatesTest < ActiveSupport::TestCase
    def setCandidate
        @candidate = Candidates.new(
            name: 'John',
            email: 'john@example.com',
            lastName: 'Tester',
            city: 'Jlle',
            postalCode: '89229101',
            coverLetter: 'no cover',
            jobId: 'abc123456'
        )
    end

    def assertAttributePresence(attrName)
        refute @candidate.valid?, "candidate is valid without a #{attrName}"
        assert_not_nil @candidate.errors[attrName], "no validation error for #{attrName} present"
    end

    test 'valid candidate' do
        setCandidate()
        assert @candidate.valid?
    end

    test 'invalid without name' do
        setCandidate()

        @candidate.name = nil;

        assertAttributePresence("name")
    end

    test 'invalid without lastName' do
        setCandidate()

        @candidate.lastName = nil;

        assertAttributePresence("lastName")
    end

    test 'invalid without email' do
        setCandidate()

        @candidate.email = nil;

        assertAttributePresence("email")
    end

    test 'invalid without city' do
        setCandidate()

        @candidate.city = nil;

        assertAttributePresence("city")
    end

    test 'invalid without postalCode' do
        setCandidate()

        @candidate.postalCode = nil;

        assertAttributePresence("postalCode")
    end

    test 'invalid without coverLetter' do
        setCandidate()

        @candidate.coverLetter = nil;

        assertAttributePresence("coverLetter")
    end
end