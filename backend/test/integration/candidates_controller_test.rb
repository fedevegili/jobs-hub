require 'test_helper'

class CandidatesControllerTest < ActionDispatch::IntegrationTest

    @@validParams = {
        "name" => "tester",
        "lastName" => "lastName",
        "email" => "email",
        "city" => "city",
        "postalCode" => "postalCode",
        "coverLetter" => "coverLetter",
        "jobId" => "abc123456"
    }

    def mockSlack
        WebMock.reset!

        @@url = "https://hooks.slack.com/services/"

        stub_request(:post, @@url)
            .to_return(
                body: {}.to_json,
                headers: {'Content-Type' => 'application/json'}
            )
    end

    test "cant create a candidate without all params" do
        mockSlack()

        post "/api/candidates", params: { "name" => "tester" }, as: :json
        assert_response :error
    end

    test "can create a candidate" do
        mockSlack()

        assert_requested(:post, @@url, times: 0)

        post "/api/candidates", params: @@validParams, as: :json

        assert_response :success

        assert_requested(:post, @@url, times: 1)
    end

    test "can not create candidate if slack api is down" do
        mockSlack()

        WebMock.reset!

        stub_request(:post, @@url).to_return(
            status: 500
        )

        post "/api/candidates", params: @@validParams, as: :json

        assert_response :error
    end
end
