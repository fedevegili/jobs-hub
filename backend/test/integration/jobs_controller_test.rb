require 'test_helper'

class JobsControllerTest < ActionDispatch::IntegrationTest

    def mockFeed
        WebMock.reset!

        stub_request(:get, "https://careers.jobscore.com/jobs/jobscore/feed.json")
            .to_return(
                body: {
                    "jobs" => [
                        {"id" => "a123"},
                        {"id" => "a456"},
                    ]
                }.to_json,
                headers: {'Content-Type' => 'application/json'}
            )
    end

    test "can render all jobs" do
        mockFeed()

        get "/api/jobs", as: :json
        assert_response :success

        json = JSON.parse(response.body)

        assert_equal json['jobs'][0]['id'], "a123"
        assert_equal json['jobs'][1]['id'], "a456"
    end

    test "can render a single job" do
        mockFeed()

        get "/api/jobs/a123", as: :json
        assert_response :success

        json = JSON.parse(response.body)

        assert_equal json['job']['id'], "a123"
    end

    test "can fail to get feed data" do
        WebMock.reset!

        stub_request(:get, "https://careers.jobscore.com/jobs/jobscore/feed.json")
            .to_return(
                status: 500
            )

        get "/api/jobs/a123", as: :json
        assert_response :error

        json = JSON.parse(response.body)

        assert_equal json['message'], "failed to communicate with jobScore api, try again later"
    end

end
