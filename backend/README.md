## Backend

### Framework choice
Ruby on rails 5 in API mode was used as the base framework. ActiveRecord support was opt-out in the create process.

### Features
* REST endpoints
* JobScore feed data caching

### Routes
![routes list](https://i.imgur.com/43yX1UA.png)

### Testing
Minitest was used as the testing framework.
Code coverage of 100% is ensured in the build process.

The coverage report can be seen in [https://fedevegili.gitlab.io/jobs/coverage-rails/](https://fedevegili.gitlab.io/jobs/coverage-rails/). It is automatically published on every merge.

### Secrets
Environment variables were used to provide code secret information. The variables necessary are:
* SLACK_HOOK (slack hook url)
* RAILS_SECRET_KEY (secret key for rails production)

### Pipelines
A pipeline system was set-up to test and publish static content.

Static content being published on every merge are:
* [coverage report](https://fedevegili.gitlab.io/jobs/coverage-rails/)

### Dependencies
* [HTTParty](https://github.com/jnunemaker/httparty) was used to make all requests
* [webmock](https://github.com/bblimke/webmock) was used to easily mock http requests

### How to develop
##### Dependencies:
* rails version 5 
##### How to start development server?
#### `rails server`
##### How to run tests?
#### `rails test`