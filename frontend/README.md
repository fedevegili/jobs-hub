## Frontend

### Framework choice
React.js was used as the base framework and [create-react-app](http://create-react-app.dev) was used to bootstrap the project. Even though create-react-app is a little flexible, it is great to bootstrap small projects and get running quickly.

### Features
This is a list of features that were not part of the application requirements, but I developed them to make things more interesting:
* Async route loading (only required JS/CSS is loaded for each page)
* Backend response caching (jobs data is reused between navigations)
* Form building and validation (required/email fields)
* Mobile-first layout
* Container-Presentation component separation to decouple requests logic
* Single page application with a modern routing system
* Minified JS/CSS provided by webpack

### Components
The list of components created is available at [https://fedevegili.gitlab.io/jobs/storybook](https://fedevegili.gitlab.io/jobs/storybook).

As seen in the [tasks list](https://gitlab.com/fedevegili/jobs/blob/master/TASKS.md), I developed the components first and later on put all together.

I believe this approach helps to ensure reusability and provides code decoupling.

I used [storybook](https://storybook.js.org) as a quick component showcase. Every component contains a page for every variation. This makes prototyping and testing very efficient, as you can quickly reach every component available.

### Testing
[Jest](http://jestjs.io) comes bootstrapped in create-react-app, so all tests were wrote using their assertions.
Code coverage of 100% is ensured in the build process.

The coverage report can be seen in [https://fedevegili.gitlab.io/jobs/coverage/](https://fedevegili.gitlab.io/jobs/coverage/). It is automatically published on every merge.

### Styling
[css modules](https://github.com/css-modules/css-modules) were used to avoid global CSS classes.

SASS was used to providing a few variables and mixings. Even though React component-based system doesn't require much advanced SASS, the most usage can be seen in the [Button component](https://gitlab.com/fedevegili/jobs/tree/master/frontend/src/components/Button/Button.module.scss).

Only a CSS normalizer ([sanitize.css](https://github.com/csstools/sanitize.css)) was used, the rest being developed by me with a mobile-first mindset.

### Code convention
[eslint](https://eslint.org/) is used to ensure that the code-base follows a convention. It is ensured in the pipeline build process.

### Pipelines
A pipeline system was set-up to build, test and publish static content.

Static content being published on every merge are:
* [storybook component showcase](https://fedevegili.gitlab.io/jobs/storybook)
* [coverage report](https://fedevegili.gitlab.io/jobs/coverage/)

### Dependencies
As the goal here is to evaluate code, I tried to use as few external libs as possible so I could code most components and have a larger codebase.

The only external libraries used were:
* [react-spinners](https://www.npmjs.com/package/react-spinners)
* [sanitize.css](https://github.com/csstools/sanitize.css)

I used them because it is a little tricky to develop them quickly.

### How to develop
##### Dependencies:
* nodejs
##### How to start development server?
#### `npm start`
##### How to start storybook server?
#### `npm run storybook`
##### How to run tests?
#### `npm run test`

### Building for production
#### `npm run build && npm run build-storybook`