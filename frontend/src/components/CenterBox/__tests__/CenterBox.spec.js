import React from 'react';
import { mount } from 'enzyme';
import CenterBox from '../CenterBox';

it('should render', () => {
	const wrapper = mount(
		<CenterBox />
	);

	expect(wrapper.find(CenterBox)).toHaveLength(1);
});