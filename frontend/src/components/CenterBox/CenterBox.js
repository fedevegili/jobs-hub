import React from "react";
import styles from './CenterBox.module.scss';

export default (props) => {
	return (
		<div className={styles.wrapper}>
			<div className={styles.centerBox}>
				{props.children}
			</div>
		</div>
	);
}