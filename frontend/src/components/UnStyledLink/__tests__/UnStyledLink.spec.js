import React from 'react';
import {mount} from 'enzyme';
import UnStyledLink from '../UnStyledLink';
import {MemoryRouter} from "react-router-dom";

it('should render', () => {
	const wrapper = mount(
		<MemoryRouter>
			<UnStyledLink to={"/"} />
		</MemoryRouter>
	);

	expect(wrapper.find(UnStyledLink)).toHaveLength(1);
});