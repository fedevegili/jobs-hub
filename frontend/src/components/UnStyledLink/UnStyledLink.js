import React from "react";
import { Link } from 'react-router-dom';

const style = {
	textDecoration: 'none',
	color: 'inherit'
};

const UnStyledLink = (props) => (
	<Link style={style} {...props} />
);

UnStyledLink.displayName = "UnStyledLink";

export default UnStyledLink;