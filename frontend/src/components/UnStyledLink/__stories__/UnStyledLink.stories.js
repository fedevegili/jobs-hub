import React from 'react';
import {storiesOf} from '@storybook/react';
import UnStyledLink from '../UnStyledLink';
import {MemoryRouter} from "react-router-dom";

import '../../../style/styling';

storiesOf(UnStyledLink.displayName, module)
	.add('default', () => (
		<MemoryRouter>
			<UnStyledLink to="/">
				<span>{"sample span"}</span>
				<br/>
				<button>{"sample button"}</button>
			</UnStyledLink>
		</MemoryRouter>
	));