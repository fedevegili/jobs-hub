import React from 'react';
import {storiesOf} from '@storybook/react';
import ButtonToolbar from '../ButtonToolbar';

import '../../../style/styling';
import Button, {sizes} from "../../Button/Button";

storiesOf(ButtonToolbar.displayName, module)
	.add('default', () => (
		<ButtonToolbar
			buttons={[
				<Button>{"back"}</Button>,
				<Button size={sizes.BUTTON_MEDIUM}>{"submit"}</Button>
			]}
		/>
	));