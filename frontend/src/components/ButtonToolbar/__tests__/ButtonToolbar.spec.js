import React from 'react';
import { mount } from 'enzyme';
import ButtonToolbar from '../ButtonToolbar';

import Button from '../../Button/Button';

it('should render', () => {
	const wrapper = mount(
		<ButtonToolbar buttons={[]} />
	);

	expect(wrapper.find(ButtonToolbar)).toHaveLength(1);
});

it('should render buttons inside', () => {
	const wrapper = mount(
		<ButtonToolbar buttons={[
			<Button text={"test"} />
		]} />
	);

	expect(wrapper.find(ButtonToolbar)).toHaveLength(1);
	expect(wrapper.find(Button)).toHaveLength(1);
});

it('should render no spacer for one button', () => {
	const wrapper = mount(
		<ButtonToolbar buttons={[
			<Button text={"test"} />
		]} />
	);

	expect(wrapper.find('span')).toHaveLength(0);
});

it('should render 2 spacers for 3 buttons', () => {
	const wrapper = mount(
		<ButtonToolbar buttons={[
			<Button text={"test"} />,
			<Button text={"test"} />,
			<Button text={"test"} />
		]} />
	);

	expect(wrapper.find('span')).toHaveLength(2);
});