import React from "react";
import styles from "./ButtonToolbar.module.scss";
import PropTypes from 'prop-types';

function getBtnWithSeparator(btns) {
	const len = (btns.length - 1);

	return btns.map((btn, index) => (
		<React.Fragment key={index}>
			{btn}
			{(index !== len) ? <span className={styles.spacer}/> : null}
		</React.Fragment>
	));
}

const ButtonToolbar = (props) => {
	return (
		<div className={styles.toolbar}>
			{getBtnWithSeparator(props.buttons)}
		</div>
	);
};

ButtonToolbar.propTypes = {
	buttons: PropTypes.array.isRequired
};

ButtonToolbar.displayName = "ButtonToolbar";

export default ButtonToolbar;