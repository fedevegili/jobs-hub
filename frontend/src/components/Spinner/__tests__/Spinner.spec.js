import React from 'react';
import { mount } from 'enzyme';
import Spinner from '../Spinner';

it('should render', () => {
	const wrapper = mount(
		<Spinner />
	);

	expect(wrapper.find(Spinner)).toHaveLength(1);
});