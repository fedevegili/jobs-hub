import React from "react";
import PropTypes from 'prop-types';
import ClipLoader from 'react-spinners/ClipLoader';
import styles from './Spinner.module.scss';

const Spinner = (props) => {
	const Clip = (
		<ClipLoader
			color={"#a0a0a0"}
			{...props}
		/>
	);

	if (props.center) {
		return (
			<div
				className={styles.spinner}
				style={{height: props.size, width: props.size}}
			>
				{Clip}
			</div>
		);
	}

	return Clip;
};

Spinner.propTypes = {
	size: PropTypes.number,
	center: PropTypes.bool
};

Spinner.defaultProps = {
	size: 40
};

export default Spinner;

Spinner.displayName = "Spinner";