import React from 'react';
import {storiesOf} from '@storybook/react';
import Spinner from '../Spinner';

import '../../../style/styling';

storiesOf(Spinner.displayName, module)
	.add('default', () => (
		<Spinner />
	)).add('sizes', () => (
		<React.Fragment>
			<Spinner />
			<hr />
			<Spinner size={20} />
			<hr />
			<Spinner size={40} />
			<hr />
			<Spinner size={60} />
		</React.Fragment>
	)).add('center', () => (
		<div style={{height: 300, width: 300, background: "white", position: "relative"}}>
			<Spinner center />
		</div>
	));