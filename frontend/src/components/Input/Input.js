import React from "react";
import styles from "./Input.module.scss";
import PropTypes from 'prop-types';

function getLabel(name, title, required) {
	let labelTitle = title + ": ";

	if (required) {
		labelTitle += "*";
	}

	return (
		<label htmlFor={name}>{labelTitle}</label>
	);
}

function change(onChange, evt) {
	onChange && onChange(evt.target.value);
}

const Input = (props) => {
	let inputCls = [styles.input], errorEl = null, InputEl = 'input';

	if (props.errorMsg) {
		inputCls.push(styles.error);

		errorEl = (
			<span className={styles.errorText}>{props.errorMsg}</span>
		);
	}

	if (props.textarea) {
		InputEl = 'textarea';
	}

	return (
		<div className={styles.inputWrapper}>
			{getLabel(props.name, props.title, props.required)}
			<InputEl
				autoComplete={props.autocomplete}
				name={props.name}
				className={[inputCls.join(" ")]}
				value={props.value}
				onChange={change.bind(null, props.onChange)}
			/>
			{errorEl}
		</div>
	);
};

Input.propTypes = {
	name: PropTypes.string,
	title: PropTypes.string.isRequired,
	autocomplete: PropTypes.string,
	required: PropTypes.bool,
	textarea: PropTypes.bool,
	errorMsg: PropTypes.string,
	onChange: PropTypes.func
};

Input.displayName = "Input";

export default Input;