import React from 'react';
import {storiesOf} from '@storybook/react';
import Input from '../Input';

import '../../../style/styling';
import {action} from "@storybook/addon-actions";

storiesOf(Input.displayName, module)
	.add('default', () => (
		<React.Fragment>
			<Input title={"Name"} name={"name"} />
			<Input title={"Age"} value={"hello"} onChange={action("onChange")} name={"age"} />
			<Input title={"Email"} name={"email"} />
		</React.Fragment>
	)).add('required', () => (
		<React.Fragment>
			<Input required title={"Name"} />
		</React.Fragment>
	)).add('with error', () => (
		<React.Fragment>
			<Input required errorMsg="Required field" title={"Name"} />
		</React.Fragment>
	)).add('as textarea', () => (
		<React.Fragment>
			<Input textarea title={"cover letter"} />
			<Input textarea errorMsg="Required field" title={"cover letter"} />
		</React.Fragment>
	));