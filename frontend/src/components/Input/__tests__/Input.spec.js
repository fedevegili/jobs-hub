import React from 'react';
import { mount } from 'enzyme';
import Input from '../Input';

it('should render', () => {
	const wrapper = mount(
		<Input title={"sample title"}/>
	);

	const text = wrapper.text();

	expect(wrapper.find(Input)).toHaveLength(1);
	expect(text).toEqual(expect.stringContaining("sample"));
});

it('should render as required', () => {
	const wrapper = mount(
		<Input required title={"name"} />
	);

	const text = wrapper.text();

	expect(text).toEqual(expect.stringContaining("*"));
});

it('should render an error message', () => {
	const wrapper = mount(
		<Input errorMsg={"failed"} title={"name"} />
	);

	const text = wrapper.text();

	expect(text).toEqual(expect.stringContaining("failed"));
});

it('should render an error message', () => {
	const wrapper = mount(
		<Input errorMsg={"failed"} title={"name"} />
	);

	const text = wrapper.text();

	expect(text).toEqual(expect.stringContaining("failed"));
});

it('should NOT render as textarea by default', () => {
	const wrapper = mount(
		<Input title={"name"} />
	);

	expect(wrapper.find('textarea')).toHaveLength(0);
});

it('should render as textarea', () => {
	const wrapper = mount(
		<Input textarea title={"name"} />
	);

	expect(wrapper.find('textarea')).toHaveLength(1);
});

it('should render attr autocomplete', () => {
	const wrapper = mount(
		<Input autocomplete={"given-name"} title={"name"} />
	);

	expect(wrapper.find("input").getDOMNode().getAttribute("autocomplete")).toEqual(expect.stringContaining("given-name"));
});

it('should render attr name', () => {
	const wrapper = mount(
		<Input name={"test-name"} title={"name"} />
	);

	expect(wrapper.find("input").getDOMNode().getAttribute("name")).toEqual(expect.stringContaining("test-name"));
});

it('should render a value', () => {
	const wrapper = mount(
		<Input value={"sample-value"} onChange={() => (null)} title={"name"} />
	);

	expect(wrapper.find("input").getDOMNode().value).toEqual(expect.stringContaining("sample-value"));
});

it('typing should trigger onChange', () => {
	const spy = jest.fn();

	const wrapper = mount(
		<Input value={""} onChange={spy} title={"name"} />
	);

	wrapper.find("input").simulate("change", {
		target: {
			value: "typed text"
		}
	});

	expect(spy.mock.calls.length).toBe(1);
	expect(spy.mock.calls[0][0]).toEqual(expect.stringContaining("typed text"));
});