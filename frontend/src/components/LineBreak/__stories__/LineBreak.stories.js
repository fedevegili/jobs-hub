import React from 'react';
import {storiesOf} from '@storybook/react';
import LineBreak from '../LineBreak';

import '../../../style/styling';

storiesOf(LineBreak.displayName, module)
	.add('default', () => (
		<div style={{background: "white", height: 500, paddingTop: 50}}>
			<LineBreak />
			<LineBreak margin={10} />
			<LineBreak margin={50}	/>
			<LineBreak margin={100}	/>
		</div>
	));