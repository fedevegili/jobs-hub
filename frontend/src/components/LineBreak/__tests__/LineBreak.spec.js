import React from 'react';
import { mount } from 'enzyme';
import LineBreak from '../LineBreak';


it('should render', () => {
	const wrapper = mount(
		<LineBreak />
	);

	expect(wrapper.find(LineBreak)).toHaveLength(1);
});

it('should render with custom margin', () => {
	const wrapper = mount(
		<LineBreak margin={50} />
	);

	expect(wrapper.find(LineBreak).getDOMNode().style.marginTop).toEqual("50px");
});