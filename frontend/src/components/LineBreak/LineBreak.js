import React from "react";
import styles from "./LineBreak.module.scss";
import PropTypes from 'prop-types';

const LineBreak = (props) => {
	let customStyle;

	if (props.margin) {
		customStyle = {
			marginTop: props.margin,
			marginBottom: props.margin
		};
	}

	return (
		<hr
			className={styles.element}
			style={customStyle}
		/>
	);
};

LineBreak.propTypes = {
	margin: PropTypes.number
};

LineBreak.displayName = "LineBreak";

export default LineBreak;