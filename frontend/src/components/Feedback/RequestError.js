import Feedback from "./Feedback";
import Button, {sizes} from "../Button/Button";
import React from "react";

export default () => (
	<Feedback
		button={<Button size={sizes.BUTTON_MEDIUM} onClick={() => {window.location.reload()}}>{"Reload"}</Button>}
		text="Request could not be completed right now, please try to reload the page"
	/>
);