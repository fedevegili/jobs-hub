import React from 'react';
import { mount } from 'enzyme';
import NoJobs from '../NoJobs';
import Button from "../../Button/Button";

it('should render', () => {
	const wrapper = mount(
		<NoJobs />
	);

	expect(wrapper.find(NoJobs)).toHaveLength(1);
});

it('should try to reload', () => {
	const wrapper = mount(
		<NoJobs />
	);

	const spy = jest.fn();

	// I will not backup because it's not necessary here
	window.location.reload = spy;

	wrapper.find(Button).simulate("click");

	expect(spy).toHaveBeenCalledTimes(1);
});