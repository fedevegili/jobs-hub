import React from 'react';
import { mount } from 'enzyme';
import Feedback from '../Feedback';
import {ReactComponent as SuccessSvg} from '../../svg/success.svg';

const mockBtn = (<button />);

it('should render', () => {
	const wrapper = mount(
		<Feedback
			text="feed test"
			button={mockBtn}
		/>
	);

	const text = wrapper.text();

	expect(wrapper.find(Feedback)).toHaveLength(1);
	expect(wrapper.find('button')).toHaveLength(1);

	expect(wrapper.find('h4')).toHaveLength(1);

	expect(text).toEqual(expect.stringContaining("feed test"));
});

it('should render with svg', () => {
	const wrapper = mount(
		<Feedback
			text="feed test"
			svg={SuccessSvg}
			button={mockBtn}
		/>
	);

	expect(wrapper.find(SuccessSvg)).toHaveLength(1);

	expect(wrapper.find('h4')).toHaveLength(0);
});