import React from 'react';
import {storiesOf} from '@storybook/react';
import Feedback from '../Feedback';
import {ReactComponent as SuccessSvg} from '../../svg/success.svg';
import Button, { sizes } from '../../Button/Button';
import RequestError from "../RequestError";
import NoJobs from "../NoJobs";

import '../../../style/styling';

function PageLayoutMock(props) {
	return (
		<div style={{height: 500, minWidth: 500, background: "#fff", padding: 20}}>
			{props.children}
		</div>
	)
}

storiesOf(Feedback.displayName, module)
	.add('success', () => (
		<PageLayoutMock>
			<Feedback
				svg={SuccessSvg}
				button={<Button size={sizes.BUTTON_MEDIUM}>{"some action"}</Button>}
				text="Thank you!!!"
				/>
		</PageLayoutMock>
	))
	.add('request error', () => (
		<PageLayoutMock>
			<RequestError />
		</PageLayoutMock>
	))
	.add('no jobs', () => (
		<PageLayoutMock>
			<NoJobs />
		</PageLayoutMock>
	));