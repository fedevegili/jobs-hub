import React from "react";
import styles from "./Feedback.module.scss";
import PropTypes from 'prop-types';

const Feedback = (props) => {
	let TextEl, wrapperCls = [styles.wrapper];

	const SVG = props.svg;

	if (SVG) {
		wrapperCls.push(styles.hasSvg);
		TextEl = 'span';
	} else {
		TextEl = 'h4';
	}

	return (
		<div className={wrapperCls.join(" ")}>
			{SVG ? <SVG className={styles.svgStyle} /> : null}
			<TextEl className={styles.text}>{props.text}</TextEl>
			{props.button}
		</div>
	);
};

Feedback.propTypes = {
	svg: PropTypes.object,
	button: PropTypes.node.isRequired,
	text: PropTypes.string.isRequired
};

Feedback.displayName = "Feedback";

export default Feedback;