import Feedback from "./Feedback";
import Button, {sizes} from "../Button/Button";
import React from "react";

export default () => (
	<Feedback
		button={<Button size={sizes.BUTTON_MEDIUM} onClick={() => {window.location.reload()}}>{"Reload"}</Button>}
		text="No jobs available, please come back later"
	/>
);