import React from 'react';
import {storiesOf} from '@storybook/react';
import Form from '../Form';

import '../../../style/styling';
import {action} from "@storybook/addon-actions";
import {email, required} from "../../../helpers/formValidation/validations";
import Button from "../../Button/Button";

const fields = [
	{
		title: "First Name",
		name: "name",
		autocomplete: "given-name",
		validation: required
	},
	{
		title: "Last Name",
		name: "lastName",
		autocomplete: "family-name",
		validation: required
	},
	{
		title: "Email",
		name: "email",
		autocomplete: "email",
		validation: email
	},
	{
		title: "City",
		name: "city",
		autocomplete: "address-level2s",
		validation: required
	},
	{
		title: "Zip",
		name: "postalCode",
		autocomplete: "postal-code",
		validation: required
	},
	{
		title: "Cover Letter",
		name: "coverLetter",
		textarea: true,
		validation: required
	}
];

storiesOf(Form.displayName, module)
	.add('default', () => (
		<Form
			onSubmit={action("onSubmit")}
			fields={fields}
			buttons={[
				<Button>{"back"}</Button>
			]}
		/>
	));