import React from 'react';
import {mount} from 'enzyme';
import Form from '../Form';
import {required} from "../../../helpers/formValidation/validations";
import Input from '../../Input/Input';
import Button from "../../Button/Button";

const city = {
	title: "City",
	name: "city",
	autocomplete: "address-level2s",
	validation: required
};

const zip = {
	title: "Zip",
	name: "postalCode",
	autocomplete: "postal-code",
	validation: required
};

const name = {
	title: "Name",
	name: "name"
};

it('should render', () => {
	const wrapper = mount(
		<Form
			onSubmit={() => {}}
		/>
	);

	expect(wrapper.find(Form)).toHaveLength(1);
});

it('should render some fields', () => {
	const wrapper = mount(
		<Form
			onSubmit={() => {}}
			fields={[
				city,
				zip
			]}
		/>
	);

	expect(wrapper.find(Input)).toHaveLength(2);
});

it('should submit fields that changed', () => {
	let spy = jest.fn();
	let doneFn;

	spy.mockReturnValue({finally: (cb) => {doneFn = cb;}});

	const wrapper = mount(
		<Form
			onSubmit={spy}
			fields={[
				city,
				zip,
				name
			]}
		/>
	);

	wrapper.find("input").at(0).simulate("change", {
		target: {
			value: "text 0"
		}
	});

	wrapper.find("input").at(1).simulate("change", {
		target: {
			value: "text 1"
		}
	});

	wrapper.find(Button).simulate("click");

	expect(wrapper.find(Button).props().loading).toEqual(true);

	expect(spy.mock.calls[0][0]).toEqual({
		city: "text 0",
		postalCode: "text 1"
	});

	// simulate submit request complete
	doneFn();

	wrapper.update();

	expect(wrapper.find(Button).props().loading).toEqual(false);
});

it('should show errors on failed fields', () => {
	let spy = jest.fn();

	const wrapper = mount(
		<Form
			onSubmit={spy}
			fields={[
				city,
				zip
			]}
		/>
	);

	wrapper.find("input").at(1).simulate("change", {
		target: {
			value: "text 1"
		}
	});

	wrapper.find(Button).simulate("click");

	wrapper.update();

	expect(wrapper.find(Input).at(0).props().errorMsg).toEqual("Field required");

	expect(spy.mock.calls).toHaveLength(0);
});