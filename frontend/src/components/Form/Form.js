import React from "react";
import PropTypes from 'prop-types';

import Input from '../Input/Input';
import Button, {sizes, kinds} from "../Button/Button";
import ButtonToolbar from "../ButtonToolbar/ButtonToolbar";
import LineBreak from "../LineBreak/LineBreak";

import validate from '../../helpers/formValidation/validate';

class Form extends React.Component {

	constructor() {
		super();

		const me = this;

		me.submit = me.submit.bind(me);
		me.submitDone = me.submitDone.bind(me);

		me.state = {
			data: {},
			errors: {},
			submitting: false
		};
	}

	getValidations(fields) {
		let validations = {};

		fields.forEach((field) => {
			if (field.validation) {
				validations[field.name] = field.validation;
			}
		});

		return validations;
	}

	submitDone() {
		this.setState({
			submitting: false
		});
	}

	submit() {
		const me = this;

		const errors = validate(me.getValidations(me.props.fields), me.state.data);

		let submitting = false;

		if (!errors) {
			submitting = true;

			me.props.onSubmit(me.state.data).finally(me.submitDone);
		}

		me.setState({
			errors: errors,
			submitting: submitting
		});
	}

	updateFieldValue(name, value) {
		var me = this;

		me.setState((state) => {
			let item = {};
			item[name] = value;

			let newData = Object.assign(
				state.data,
				item
			);

			return {
				data: newData
			};
		})
	}

	getFormField(fieldProps) {
		const me = this, name = fieldProps.name;

		return (
			<Input
				key={name}
				required
				errorMsg={me.state.errors ? me.state.errors[name] : null}
				value={me.state.data[name] || ""}
				onChange={me.updateFieldValue.bind(me, name)}
				{...fieldProps}
			/>
		);
	}

	render() {
		var me = this;

		return (
			<div>
				{me.props.fields.map((field) => (
					me.getFormField(field)
				))}
				<LineBreak />
				<ButtonToolbar
					buttons={me.props.buttons.concat([
						<Button
							loading={me.state.submitting}
							kind={kinds.BUTTON_PRIMARY}
							size={sizes.BUTTON_MEDIUM}
							onClick={me.submit}
						>
							{"Submit"}
						</Button>
					])}
				/>
			</div>
		);
	}
};

Form.defaultProps = {
	fields: [],
	buttons: []
};

Form.propTypes = {
	onSubmit: PropTypes.func.isRequired,
	fields: PropTypes.array.isRequired,
	buttons: PropTypes.array
};

Form.displayName = "Form";

export default Form;