import React from "react";
import styles from "./Alert.module.scss";
import PropTypes from 'prop-types';

function getItemList(list) {
	if (!list) {
		return null;
	}

	return (
		<ul className={styles.listWrapper}>
			{list.map((item, index) => (
				<li key={index} className={styles.listItem}>{item}</li>
			))}
		</ul>
	);
}

const Alert = (props) => {
	return (
		<div className={styles.container}>
			<h5 className={styles.text}>{props.text}</h5>
			{getItemList(props.list)}
			<span className={styles.dismiss} onClick={props.onDismiss}>{"×"}</span>
		</div>
	);
};

Alert.propTypes = {
	text: PropTypes.string,
	list: PropTypes.arrayOf(PropTypes.string),
	onDismiss: PropTypes.func
};

Alert.displayName = "Alert";

export default Alert;