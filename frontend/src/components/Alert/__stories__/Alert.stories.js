import React from 'react';
import {storiesOf} from '@storybook/react';
import Alert from '../Alert';

import '../../../style/styling';
import {action} from "@storybook/addon-actions";

storiesOf(Alert.displayName, module)
	.add('default', () => (
		<Alert
			text={"Some text goes here"}
			onDismiss={action("dismiss")}
		/>
	)).add('with a list', () => (
		<Alert
			text={"Some text goes here"}
			list={[
				"fix 1",
				"fix 2"
			]}
			onDismiss={action("dismiss")}
		/>
	));