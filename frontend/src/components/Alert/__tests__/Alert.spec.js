import React from 'react';
import { mount } from 'enzyme';
import Alert from '../Alert';

const sampleStr = "sample";

it('should render', () => {
	const wrapper = mount(
		<Alert text={sampleStr} />
	);

	const text = wrapper.text();

	expect(text).toEqual(expect.stringContaining(sampleStr));

	expect(wrapper.find(Alert)).toHaveLength(1);
});

it('should render a list', () => {
	const wrapper = mount(
		<Alert
			text={sampleStr}
			list={[
				"fix 1",
				"fix 2"
			]}
		/>
	);

	const text = wrapper.text();

	expect(text).toEqual(expect.stringContaining("fix 1"));
	expect(text).toEqual(expect.stringContaining("fix 2"));

	expect(wrapper.find(Alert)).toHaveLength(1);
});