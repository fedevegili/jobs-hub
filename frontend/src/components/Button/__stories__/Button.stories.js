import React from 'react';
import {action} from '@storybook/addon-actions';
import {storiesOf} from '@storybook/react';
import Button, {sizes, kinds} from '../Button';

import '../../../style/styling';

const separator = (<span style={{height: 10, width: 20, display: "inline-block"}} />);

function getKinds(props) {
    return (
        <React.Fragment>
            <Button
                kind={kinds.BUTTON_PRIMARY}
                {...props}
                onClick={action("clicked")}
            >
                {"primary"}
            </Button>
            {separator}
            <Button
                kind={kinds.BUTTON_SECONDARY}
                {...props}
                onClick={action("clicked")}
            >
                {"secondary"}
            </Button>
            {separator}
            <Button
                kind={kinds.BUTTON_ATTENTION}
                {...props}
                onClick={action("clicked")}
            >
                {"attention"}
            </Button>
        </React.Fragment>
    )
}

storiesOf(Button.displayName, module)
	.add('default', () => (
		<Button onClick={action("clicked")}>{"sample button"}</Button>
	))
	.add('sizes', () => (
		<React.Fragment>
			<Button
                size={sizes.BUTTON_SMALL}
                onClick={action("clicked")}
            >
                {"small button"}
			</Button>
			{separator}
			<Button
                size={sizes.BUTTON_MEDIUM}
                onClick={action("clicked")}
            >
                {"medium button"}
			</Button>
		</React.Fragment>
	))
	.add('kinds', () => (
		<React.Fragment>
            {getKinds({size: sizes.BUTTON_SMALL})}
            <hr />
            {getKinds({size: sizes.BUTTON_MEDIUM})}
		</React.Fragment>
	))
	.add('disabled', () => (
		<React.Fragment>
			{getKinds({disabled: true})}
		</React.Fragment>
	))
	.add('loading', () => (
		<React.Fragment>
			<Button loading onClick={action("clicked")}>{"loading"}</Button>
			{separator}
			<Button loading size={sizes.BUTTON_MEDIUM} onClick={action("clicked")}>{"loading"}</Button>
		</React.Fragment>
	));
