import React from "react";
import styles from './Button.module.scss';
import PropTypes from 'prop-types';
import Spinner from '../Spinner/Spinner';

function getSizeClass(size) {
	switch (size) {
		case sizes.BUTTON_SMALL:
			return styles.sizeSmall;
		case sizes.BUTTON_MEDIUM:
			return styles.sizeMedium;
		default:
			return styles.sizeSmall;
	}
}

function getKindClass(kind) {
	switch (kind) {
		case kinds.BUTTON_SECONDARY:
			return styles.kindSecondary;
		case kinds.BUTTON_PRIMARY:
			return styles.kindPrimary;
		case kinds.BUTTON_ATTENTION:
			return styles.kindAttention;
		default:
			return styles.kindSecondary;
	}
}

const Button = function(props) {
	let innerContent = props.children, disabled = props.disabled;

	const clsName = [
		styles.Button,
		getSizeClass(props.size),
		getKindClass(props.kind)
	];

	if (props.loading) {
		disabled = true;

		innerContent = (<Spinner size={20} />);
	}

	if (disabled) {
		clsName.push(styles.disabled);
	}

	return (
		<button
			className={clsName.join(" ")}
			onClick={disabled ? null : props.onClick}
		>
			{innerContent}
		</button>
	);
};

export default Button;

export const sizes = {
	BUTTON_SMALL: 1,
	BUTTON_MEDIUM: 2
};

export const kinds = {
	BUTTON_PRIMARY: 1,
	BUTTON_SECONDARY: 2,
	BUTTON_ATTENTION: 3
};

Button.propTypes = {
	size: PropTypes.oneOf(Object.values(sizes)),
	kind: PropTypes.oneOf(Object.values(kinds)),
	disabled: PropTypes.bool,
	loading: PropTypes.bool,
	onClick: PropTypes.func
};

Button.displayName = "Button";