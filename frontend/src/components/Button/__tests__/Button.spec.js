import React from 'react';
import { mount } from 'enzyme';
import Button, {sizes, kinds} from '../Button';
import Spinner from '../../Spinner/Spinner';

it('should render', () => {
	const wrapper = mount(
		<Button
		/>
	);

	expect(wrapper.find(Button)).toHaveLength(1);
});

it('should render a small button', () => {
	const wrapper = mount(
		<Button size={sizes.BUTTON_SMALL}>{"test"}</Button>
	);

	expect(wrapper.find('.sizeSmall')).toHaveLength(1);
});

it('should render a medium button', () => {
	const wrapper = mount(
		<Button size={sizes.BUTTON_MEDIUM}>{"test"}</Button>
	);

	expect(wrapper.find('.sizeMedium')).toHaveLength(1);
});

it('should render a medium primary button', () => {
	const wrapper = mount(
		<Button size={sizes.BUTTON_MEDIUM} kind={kinds.BUTTON_PRIMARY}>{"test"}</Button>
	);

	expect(wrapper.find('.sizeMedium')).toHaveLength(1);
	expect(wrapper.find('.kindPrimary')).toHaveLength(1);
});

it('should render a small attention button', () => {
	const wrapper = mount(
		<Button size={sizes.BUTTON_SMALL} kind={kinds.BUTTON_ATTENTION}>{"test"}</Button>
	);

	expect(wrapper.find('.sizeSmall')).toHaveLength(1);
	expect(wrapper.find('.kindAttention')).toHaveLength(1);
});

it('should render a small secondary button', () => {
	const wrapper = mount(
		<Button size={sizes.BUTTON_SMALL} kind={kinds.BUTTON_SECONDARY}>{"test"}</Button>
	);

	expect(wrapper.find('.sizeSmall')).toHaveLength(1);
	expect(wrapper.find('.kindSecondary')).toHaveLength(1);
});

it('click should work', () => {
	const spy = jest.fn();

	const wrapper = mount(
		<Button onClick={spy}>{"test"}</Button>
	);

	wrapper.simulate('click');

	expect(spy).toHaveBeenCalled();
});

it('click should not work if disabled', () => {
	const spy = jest.fn();

	const wrapper = mount(
		<Button disabled onClick={spy}>{"test"}</Button>
	);

	wrapper.simulate('click');

	expect(spy).toHaveBeenCalledTimes(0);
});

it('should render loading', () => {
	const spy = jest.fn();

	const wrapper = mount(
		<Button loading onClick={spy}>{"test"}</Button>
	);

	wrapper.simulate('click');

	expect(wrapper.find(Spinner)).toHaveLength(1);
	expect(spy).toHaveBeenCalledTimes(0);
});