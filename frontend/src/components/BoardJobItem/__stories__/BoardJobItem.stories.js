import React from 'react';
import {storiesOf} from '@storybook/react';
import BoardJobItem from '../BoardJobItem';

import '../../../style/styling';

storiesOf(BoardJobItem.displayName, module)
	.add('default', () => (
		<React.Fragment>
			<BoardJobItem
				title={"Lead Designer"}
				department={"UX"}
				location={"London, England"}
			/>
			<BoardJobItem
				title={"Senior Frontend Developer"}
				department={"Engineering"}
				location={"Joinville, SC, Brazil"}
			/>
		</React.Fragment>
	));