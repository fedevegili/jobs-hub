import React from "react";
import styles from "./BoardJobItem.module.scss";
import PropTypes from 'prop-types';

const BoardJobItem = (props) => {
	return (
		<div className={styles.container}>
			<div className={styles.itemHeader}>
				<h4 className={styles.itemHeaderTitle}>{props.title}</h4>
				<span>{props.department}</span>
			</div>
			<div>
				<span>{props.location}</span>
			</div>
		</div>
	);
};

BoardJobItem.propTypes = {
	title: PropTypes.string.isRequired,
	department: PropTypes.string.isRequired,
	location: PropTypes.string.isRequired
};

BoardJobItem.displayName = "BoardJobItem";

export default BoardJobItem;