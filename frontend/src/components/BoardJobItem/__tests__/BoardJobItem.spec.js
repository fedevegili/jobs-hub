import React from 'react';
import { mount } from 'enzyme';
import BoardJobItem from '../BoardJobItem';

it('should render', () => {
	const wrapper = mount(
		<BoardJobItem
			title={"backend developer"}
			department={"development"}
			location={"somewhere"}
		/>
	);

	const text = wrapper.text();

	expect(wrapper.find(BoardJobItem)).toHaveLength(1);

	expect(text).toEqual(expect.stringContaining("backend developer"));
	expect(text).toEqual(expect.stringContaining("development"));
	expect(text).toEqual(expect.stringContaining("somewhere"));
});