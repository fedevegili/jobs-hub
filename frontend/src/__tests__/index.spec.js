beforeEach(() => {
	const div = document.createElement('div');

	div.id = "root";

	document.body.appendChild(div);
});

afterEach(() => {
	document.getElementById("root").remove();
});

it('renders without crashing', () => {
	require('../index');
});