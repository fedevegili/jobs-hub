export default (fields, data) => {
	let errors, key;

	for (key in fields) {
		let field = fields[key];
		let val = data[key];

		let validation = field(val);

		if (validation) {
			if (!errors) errors = {};

			errors[key] = validation;
		}
	}

	return errors;
}