import validate from '../validate';

import {email, required} from '../validations';

it('should validate no fields', () => {
	let errors = validate();

	expect(errors).toBeUndefined();
});

it('should validate a few fields', () => {
	let errors = validate({
		name: required
	}, {
		name: "mock"
	});

	expect(errors).toBeUndefined();
});

it('should return an error object if name is not defined', () => {
	let errors = validate({
		name: required
	}, {});

	expect(errors).toEqual({
		name: "Field required"
	});
});

it('should return an error object if multiple keys are not defined', () => {
	let errors = validate({
		name: required,
		zip: required,
		city: required,
		phone: required
	}, {
		zip: "123",
		city: "Joinville"
	});

	expect(errors).toEqual({
		name: "Field required",
		phone: "Field required"
	});
});

it('should validate email', function() {
	let errors = validate({
		email1: email,
		email2: email
	}, {
		email1: "sample@test.com",
		email2: "sample"
	});

	expect(errors).toEqual({
		email2: "Email invalid"
	});
});

