import React, { lazy, Suspense } from "react";
import Spinner from "../components/Spinner/Spinner";

export default function(getAsyncCmp, routeProps) {
	const LazyCmp = lazy(() => getAsyncCmp());

	return (props) => {
		return (
			<Suspense fallback={(<Spinner center />)}>
				<LazyCmp {...props} {...routeProps} />
			</Suspense>
		);
	};
}