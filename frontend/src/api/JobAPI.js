function findJob(jobs, id) {
	let len;

	if (!jobs) {
		return;
	}

	len = jobs.length;

	for (let count = 0; count < len; count++) {
		if (jobs[count].id === id) {
			return jobs[count];
		}
	}
}

function returnResolvedPromise(data) {
	return new Promise((resolve) => {
		resolve(data);
	});
}

class JobAPI {
	constructor() {
		const me = this;

		me.jobs = undefined;
	}

	fetchJobs() {
		const me = this;

		if (me.jobs) {
			return returnResolvedPromise(me.jobs);
		}

		return fetch('/api/jobs')
			.then(response => response.json())
			.then(result => result.jobs)
			.then((jobs) => {
				me.jobs = jobs;

				return jobs;
			});
	}

	fetchJob(id) {
		const me = this, cachedJob = findJob(me.jobs, id);

		if (cachedJob) {
			return returnResolvedPromise(cachedJob);
		}

		return fetch(`/api/jobs/${id}`)
			.then(response => response.json())
			.then(result => result.job)
	}

	applyForJob(id, data) {
		let newData = Object.assign(
			{},
			data,
			{jobId: id}
		);

		return fetch(
			`/api/candidates`,
			{
				method: 'POST',
				headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json'
				},
				body: JSON.stringify(newData)
			}
		);
	}
}

export default JobAPI;