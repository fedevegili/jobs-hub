import JobAPI from "../JobAPI";
import {successRequest} from "../../__mocks__/requestMock";
import {jobA, jobB} from "../../__mocks__/jobData";

var fetchBkp;

beforeAll(() => {
	fetchBkp = global.fetch;
});

afterAll(() => {
	global.fetch = fetchBkp;
});

it("should fetch jobs only once", function(async) {
	const api = new JobAPI();
	const spy = jest.fn();
	const jobs = [jobA];
	const results = [];

	global.fetch = () => {
		spy.apply(null, arguments);

		return successRequest({
			jobs: jobs}
		).apply(null, arguments);
	};

	api.fetchJobs().then((data) => {results.push(data)});

	setTimeout(() => {
		api.fetchJobs().then((data) => {results.push(data)});
		api.fetchJobs().then((data) => {results.push(data)});

		expect(spy).toHaveBeenCalledTimes(1);

		setTimeout(() => {
			expect(results).toHaveLength(3);
			expect(results[0]).toEqual(jobs);
			expect(results[1]).toEqual(jobs);
			expect(results[2]).toEqual(jobs);

			async();
		});
	})
});

it("should use previously fetched job", function(async) {
	const api = new JobAPI();
	const spy = jest.fn();
	const results = [];
	const jobs = [jobA];

	global.fetch = () => {
		spy.apply(null, arguments);

		return successRequest({
			jobs: jobs}
		).apply(null, arguments);
	};

	api.fetchJobs();

	setTimeout(() => {
		api.fetchJob('a').then((data) => {results.push(data)});
		api.fetchJob('a').then((data) => {results.push(data)});

		setTimeout(() => {
			expect(spy).toHaveBeenCalledTimes(1);

			expect(results[0]).toEqual(jobs[0]);
			expect(results[1]).toEqual(jobs[0]);

			async();
		});
	})
});

it("should fetch job", function(async) {
	const api = new JobAPI();
	const jobs = [jobA];
	const results = [];

	global.fetch = successRequest({jobs: jobs});

	api.fetchJobs();

	setTimeout(() => {
		global.fetch = successRequest({job: jobB});

		debugger;
		api.fetchJob('b').then((data) => {results.push(data)});

		setTimeout(() => {
			expect(results[0]).toEqual(jobB);

			async();
		});
	});
});