import React from 'react';
import { shallow } from 'enzyme';
import withJobs from "../withJobs";
import JobAPI from "../../api/JobAPI";
import {failedRequest, successRequest} from "../../__mocks__/requestMock";

let fetchBkp;

const SampleCmp = function() {
	return (<div />);
};

const HOCSample = withJobs(SampleCmp);

beforeAll(() => {
	fetchBkp = global.fetch;
});

afterAll(() => {
	global.fetch = fetchBkp;
});

it('should fetch jobs successfully', (async) => {
	global.fetch = successRequest({jobs: [{id: 'abc'}]});

	const wrapper = shallow(
		<HOCSample jobAPI={new JobAPI()} />
	);

	setTimeout(() => {
		wrapper.update();

		expect(wrapper.props().jobs[0].id).toEqual("abc");

		async();
	});
});

it('should fail to fetch jobs', (async) => {
	global.fetch = failedRequest();

	const wrapper = shallow(
		<HOCSample jobAPI={new JobAPI()} />
	);

	setTimeout(() => {
		wrapper.update();

		expect(wrapper.props().error).toEqual(true);

		async();
	});
});

it('should fetch a single job successfully', (async) => {
	global.fetch = successRequest({job: {id: 'abc'}});

	const wrapper = shallow(
		<HOCSample jobAPI={new JobAPI()} jobId={"abc"} />
	);

	setTimeout(() => {
		wrapper.update();

		expect(wrapper.props().job.id).toEqual("abc");

		async();
	});
});