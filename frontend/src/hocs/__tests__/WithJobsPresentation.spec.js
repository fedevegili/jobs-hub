import React from 'react';
import {mount} from 'enzyme';
import WithJobsPresentation from '../__presentation__/WithJobsPresentation';
import Spinner from "../../components/Spinner/Spinner";
import RequestError from "../../components/Feedback/RequestError";
import Feedback from "../../components/Feedback/Feedback";

var SampleCmp = () => (<div/>);

it('should render an empty state', () => {
	const wrapper = mount(
		<WithJobsPresentation jobs={[]} />
	);

	expect(wrapper.find(Feedback)).toHaveLength(1);
	expect(wrapper.find(WithJobsPresentation)).toHaveLength(1);
});

it('should render loading', () => {
	const wrapper = mount(
		<WithJobsPresentation jobs={[]} loading />
	);

	expect(wrapper.find(Spinner)).toHaveLength(1);
});

it('should render an error msg', () => {
	const wrapper = mount(
		<WithJobsPresentation jobs={[]} error />
	);

	expect(wrapper.find(RequestError)).toHaveLength(1);
});

it('should render the job component', () => {
	const wrapper = mount(
		<WithJobsPresentation HOC={SampleCmp} jobs={[]} job={{id: 123}} />
	);

	expect(wrapper.find(SampleCmp)).toHaveLength(1);
});

it('should render the jobs component', () => {
	const wrapper = mount(
		<WithJobsPresentation HOC={SampleCmp} jobs={[{id: 123}]} />
	);

	expect(wrapper.find(SampleCmp)).toHaveLength(1);
});