import React from 'react';
import RequestError from "../../components/Feedback/RequestError";
import Spinner from "../../components/Spinner/Spinner";
import NoJobs from "../../components/Feedback/NoJobs";

export default (props) => {
	const HOC = props.HOC;

	if (props.error) {
		return (
			<RequestError />
		);
	}

	if (props.loading) {
		return (
			<Spinner center />
		);
	}

	if (props.jobs.length === 0 && !props.job) {
		return (
			<NoJobs />
		);
	}

	return (
		<HOC {...props} />
	);
};