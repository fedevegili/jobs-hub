import React from 'react';
import PropTypes from "prop-types";
import JobAPI from "../api/JobAPI";
import WithJobsPresentation from "./__presentation__/WithJobsPresentation";

export default (HOCComponent) => {

	class WithJob extends React.Component {
		constructor() {
			super();

			const me = this;

			me.state = {
				jobs: [],
				loading: true,
				error: undefined
			};

			me.setJobs = me.setJobs.bind(me);
			me.setJob = me.setJob.bind(me);
			me.setFailedState = me.setFailedState.bind(me);
			me.fetchJobs = me.fetchJobs.bind(me);
		}

		setJobs(jobs) {
			const me = this;

			me.setState({
				jobs: jobs
			});
		}

		setJob(job) {
			const me = this;

			me.setState({
				job: job
			});
		}

		setFailedState(error) {
			const me = this;

			me.setState({
				error: true
			});
		}

		fetchJobs() {
			const me = this,
				props = me.props,
				jobAPI = me.props.jobAPI;

			let chain;

			if (props.jobId) {
				chain = jobAPI.fetchJob(props.jobId).then(me.setJob);
			} else {
				chain = jobAPI.fetchJobs().then(me.setJobs);
			}

			chain
				.catch(me.setFailedState)
				.finally(() => {me.setState({loading: false})})
		}

		componentDidMount() {
			this.fetchJobs();
		}

		render() {
			const me = this, state = me.state;

			return (
				<WithJobsPresentation
					HOC={HOCComponent}
					jobs={state.jobs}
					job={state.job}
					loading={state.loading}
					error={state.error}
					{...me.props}
				/>
			);
		}
	}

	WithJob.propTypes = {
		jobAPI: PropTypes.instanceOf(JobAPI).isRequired,
		jobId: PropTypes.string
	};

	return WithJob;
};