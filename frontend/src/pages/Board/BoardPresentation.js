import React from "react";
import styles from './Board.module.scss';
import BoardJobItem from '../../components/BoardJobItem/BoardJobItem';
import UnStyledLink from '../../components/UnStyledLink/UnStyledLink';
import PropTypes from 'prop-types';
import {ROUTE_JOB_INFO_FN} from "../routes";

function getJob(job) {
	return (
		<UnStyledLink to={ROUTE_JOB_INFO_FN(job.id)} key={job.id}>
			<BoardJobItem
				title={job.title}
				department={job.department}
				location={job.location}
			/>
		</UnStyledLink>
	);
}

function BoardPresentation(props) {
	return (
		<div>
			<h4 className={styles.announcement}>{"Available jobs at JobScore"}</h4>
			{props.jobs.map(getJob)}
		</div>
	);
}

BoardPresentation.propTypes = {
	jobs: PropTypes.array.isRequired
};

export default BoardPresentation;