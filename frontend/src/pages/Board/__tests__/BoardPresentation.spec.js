import React from 'react';
import { mount } from 'enzyme';
import BoardPresentation from '../BoardPresentation';
import { MemoryRouter } from "react-router-dom";
import BoardJobItem from "../../../components/BoardJobItem/BoardJobItem";
import {jobA, jobB} from "../../../__mocks__/jobData";

it('should render some jobs', () => {
	const wrapper = mount(
		<MemoryRouter>
			<BoardPresentation jobs={[jobA, jobB]}
			/>
		</MemoryRouter>
	);

	expect(wrapper.find(BoardJobItem)).toHaveLength(2);
});