import React from 'react';
import { mount } from 'enzyme';
import BoardContainer from '../BoardContainer';
import JobAPI from "../../../api/JobAPI";
import { MemoryRouter } from "react-router-dom";

it('should render', () => {
	const wrapper = mount(
		<MemoryRouter>
			<BoardContainer jobAPI={new JobAPI()} />
		</MemoryRouter>
	);

	expect(wrapper.find(BoardContainer)).toHaveLength(1);
});