import BoardPresentation from './BoardPresentation';
import withJobs from "../../hocs/withJobs";

export default withJobs(BoardPresentation);