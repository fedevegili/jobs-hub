import React from 'react';
import { mount } from 'enzyme';
import Feedback from '../Applied';
import { MemoryRouter } from "react-router-dom";

it('should render', () => {
	const wrapper = mount(
		<MemoryRouter>
			<Feedback />
		</MemoryRouter>
	);

	expect(wrapper.find(Feedback)).toHaveLength(1);
});