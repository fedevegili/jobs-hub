import React from "react";
import UnStyledLink from '../../components/UnStyledLink/UnStyledLink';
import {ROUTE_JOB_LIST_FN} from '../../pages/routes';
import Button, { sizes } from '../../components/Button/Button';
import {ReactComponent as SuccessSvg} from '../../components/svg/success.svg';
import Feedback from "../../components/Feedback/Feedback";

export default () => {
	const btn = (
		<UnStyledLink to={ROUTE_JOB_LIST_FN()}>
			<Button size={sizes.BUTTON_MEDIUM}>{"Back to job list"}</Button>
		</UnStyledLink>
	);

	return (
		<Feedback
			svg={SuccessSvg}
			button={btn}
			text="Thank you for your submission!"
		/>
	);
};