import React from "react";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import CenterBox from '../../components/CenterBox/CenterBox'

import asyncLoader from '../../helpers/asyncLoader';
import JobAPI from "../../api/JobAPI";
import {ROUTE_JOB_APPLY_FN, ROUTE_JOB_FEEDBACK_FN, ROUTE_JOB_INFO_FN, ROUTE_JOB_LIST_FN} from "../routes";

class UIApp extends React.Component {
	constructor() {
		super();

		const me = this;

		me.jobAPI = new JobAPI();
	}

	render() {
		const me = this;

		const apiProps = {jobAPI: me.jobAPI};

		return (
			<BrowserRouter>
				<CenterBox>
					<Switch>
						<Route
							exact
							path={ROUTE_JOB_LIST_FN()}
							component={asyncLoader(() => import('../Board/BoardContainer'), apiProps)}
						/>
						<Route
							exact
							path={ROUTE_JOB_INFO_FN(":id")}
							component={asyncLoader(() => import('../Job/JobContainer'), apiProps)}
						/>
						<Route
							exact
							path={ROUTE_JOB_APPLY_FN(":id")}
							component={asyncLoader(() => import('../JobApply/JobApplyContainer'), apiProps)}
						/>
						<Route
							exact
							path={ROUTE_JOB_FEEDBACK_FN(":id")}
							component={asyncLoader(() => import('../Applied/Applied'))}
						/>
						<Route component={asyncLoader(() => import('../NoMatch/NoMatch'))} />
					</Switch>
				</CenterBox>
			</BrowserRouter>
		);
	}
}

export default UIApp;