import React from 'react';
import {mount} from 'enzyme';
import UIApp from '../UIApp';
import BoardContainer from "../../Board/BoardContainer";
import Spinner from "../../../components/Spinner/Spinner";
import JobContainer from "../../Job/JobContainer";
import { MemoryRouter } from "react-router-dom";
import Feedback from "../../Applied/Applied";
import JobApplyContainer from "../../JobApply/JobApplyContainer";
import NoMatch from "../../NoMatch/NoMatch";
import {failedRequest} from "../../../__mocks__/requestMock";

const rrd = require('react-router-dom');

let browserRouterBkp;
let fetchBkp;

beforeAll(() => {
	browserRouterBkp = rrd.BrowserRouter;
	fetchBkp = global.fetch;

	rrd.BrowserRouter = ({children}) => {
		return (<div>{children}</div>);
	};

	global.fetch = failedRequest();
});

afterAll(() => {
	rrd.BrowserRouter = browserRouterBkp;
	global.fetch = fetchBkp;
});

function assertLazyLoaded(assertionCmp, wrapper, async) {
	// waits for lazy loading to render
	setTimeout(() => {
		wrapper.update();

		if (wrapper.find(Spinner).length > 0) {
			return assertLazyLoaded.apply(null, arguments);
		}

		expect(wrapper.find(assertionCmp)).toHaveLength(1);

		wrapper.unmount();
		async();
	});
}

it('should render board', (async) => {
	const wrapper = mount(
		<MemoryRouter initialEntries={["/"]}>
			<UIApp/>
		</MemoryRouter>
	);

	assertLazyLoaded(BoardContainer, wrapper, async);
});

it('should render job', (async) => {
	const wrapper = mount(
		<MemoryRouter initialEntries={["/jobs/45566"]}>
			<UIApp/>
		</MemoryRouter>
	);

	assertLazyLoaded(JobContainer, wrapper, async);
});

it('should render feedback', (async) => {
	const wrapper = mount(
		<MemoryRouter initialEntries={["/jobs/45566/feedback"]}>
			<UIApp/>
		</MemoryRouter>
	);

	assertLazyLoaded(Feedback, wrapper, async);
});

it('should render apply page', (async) => {
	const wrapper = mount(
		<MemoryRouter initialEntries={["/jobs/45566/apply"]}>
			<UIApp/>
		</MemoryRouter>
	);

	assertLazyLoaded(JobApplyContainer, wrapper, async);
});

it('should render not found', (async) => {
	const wrapper = mount(
		<MemoryRouter initialEntries={["/random"]}>
			<UIApp/>
		</MemoryRouter>
	);

	assertLazyLoaded(NoMatch, wrapper, async);
});