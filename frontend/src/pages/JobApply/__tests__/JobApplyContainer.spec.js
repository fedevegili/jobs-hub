import React from 'react';
import { mount } from 'enzyme';
import JobApplyContainer from '../JobApplyContainer';
import JobAPI from "../../../api/JobAPI";
import {successPromiseFailedRequest, successRequest} from "../../../__mocks__/requestMock";

var fetchBkp;

beforeAll(() => {
	fetchBkp = global.fetch;
});

afterAll(() => {
	global.fetch = fetchBkp;
});

it('should render', () => {
	const wrapper = mount(
		<JobApplyContainer match={{params: {id: 'abc'}}} jobAPI={new JobAPI()} />
	);

	expect(wrapper.children().props().jobId).toEqual('abc');
});

it('failed submit should set an submitErrorMsg on children', (async) => {
	global.fetch = successPromiseFailedRequest("failed");

	const wrapper = mount(
		<JobApplyContainer match={{params: {id: 'abc'}}} jobAPI={new JobAPI()} />
	);

	wrapper.children().props().onSubmit({sample: 123});

	setTimeout(() => {
		wrapper.update();

		expect(wrapper.children().props().submitErrorMsg).toEqual("failed");

		// next submit should clear submitErrorMsg
		wrapper.children().props().onSubmit({sample: 123});

		wrapper.update();

		expect(wrapper.children().props().submitErrorMsg).toEqual(undefined);

		async();
	});
});

it('success submit should try to redirect', (async) => {
	global.fetch = successRequest({});

	const wrapper = mount(
		<JobApplyContainer match={{params: {id: 'abc'}}} jobAPI={new JobAPI()} />
	);

	wrapper.children().props().onSubmit({sample: 123});

	setTimeout(() => {
		wrapper.update();

		expect(wrapper.children().props().redirect).toEqual(true);

		async();
	});
});

