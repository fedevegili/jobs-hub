import React from 'react';
import { mount } from 'enzyme';
import JobApplyPresentation from '../JobApplyPresentation';
import { MemoryRouter } from "react-router-dom";
import {jobA} from "../../../__mocks__/jobData";
import Alert from "../../../components/Alert/Alert";


it('should render', () => {
	const wrapper = mount(
		<MemoryRouter>
			<JobApplyPresentation job={jobA} onSubmit={() => {}} />
		</MemoryRouter>
	);

	expect(wrapper.find(JobApplyPresentation)).toHaveLength(1);
});

it('should render an alert', () => {
	const wrapper = mount(
		<MemoryRouter>
			<JobApplyPresentation
				job={jobA}
				onSubmit={() => {}}
				submitErrorMsg={"failed"}
			/>
		</MemoryRouter>
	);

	expect(wrapper.find(Alert)).toHaveLength(1);
});

it('should try to redirect', () => {
	const wrapper = mount(
		<MemoryRouter>
			<JobApplyPresentation
				job={jobA}
				onSubmit={() => {}}
				redirect
			/>
		</MemoryRouter>
	);

	expect(wrapper.children().props().history.location.pathname).toBe("/jobs/a/feedback");
});

it('dismissing alert should call onDismissSubmitError', () => {
	const spy = jest.fn();

	const wrapper = mount(
		<MemoryRouter>
			<JobApplyPresentation
				job={jobA}
				onSubmit={() => {}}
				onDismissSubmitError={spy}
				submitErrorMsg={"failed"}
			/>
		</MemoryRouter>
	);

	wrapper.find(Alert).find('span').simulate('click');

	expect(spy).toHaveBeenCalledTimes(1);
});