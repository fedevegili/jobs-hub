import React from "react";
import styles from './JobApply.module.scss';
import Button from '../../components/Button/Button';
import PropTypes from "prop-types";
import UnStyledLink from "../../components/UnStyledLink/UnStyledLink";
import {ROUTE_JOB_FEEDBACK_FN, ROUTE_JOB_INFO_FN} from "../routes";
import Form from "../../components/Form/Form";
import {email, required} from "../../helpers/formValidation/validations";
import Alert from "../../components/Alert/Alert";
import LineBreak from "../../components/LineBreak/LineBreak";
import { Redirect } from 'react-router';

const fields = [
	{
		title: "First Name",
		name: "name",
		autocomplete: "given-name",
		validation: required
	},
	{
		title: "Last Name",
		name: "lastName",
		autocomplete: "family-name",
		validation: required
	},
	{
		title: "Email",
		name: "email",
		autocomplete: "email",
		validation: email
	},
	{
		title: "City",
		name: "city",
		autocomplete: "address-level2s",
		validation: required
	},
	{
		title: "Zip",
		name: "postalCode",
		autocomplete: "postal-code",
		validation: required
	},
	{
		title: "Cover Letter",
		name: "coverLetter",
		textarea: true,
		validation: required
	}
];

function getErrorAlert(submitErrorMsg, onDismissSubmitError) {
	if (!submitErrorMsg) {
		return null;
	}

	return (
		<React.Fragment>
			<LineBreak />
			<Alert
				text={submitErrorMsg}
				onDismiss={onDismissSubmitError}
			/>
			<LineBreak />
		</React.Fragment>
	);
}

function getRedirect(redirect, jobId) {
	if (!redirect) {
		return null;
	}

	return (
		<Redirect push to={ROUTE_JOB_FEEDBACK_FN(jobId)} />
	);
}

function JobApplyPresentation(props) {
	const jobId = props.job.id;

	const backBtn = (
		<UnStyledLink to={ROUTE_JOB_INFO_FN(jobId)}>
			<Button>{"back"}</Button>
		</UnStyledLink>
	);

	return (
		<div className={styles.wrapper}>
			<h2 className={styles.jobTitle}>{props.job.title}</h2>
			<h4>{"Contact information"}</h4>
			{getErrorAlert(props.submitErrorMsg, props.onDismissSubmitError)}
			<Form
				fields={fields}
				buttons={[backBtn]}
				onSubmit={props.onSubmit}
			/>
			{getRedirect(props.redirect, jobId)}
		</div>
	);
}

JobApplyPresentation.propTypes = {
	job: PropTypes.object,
	onSubmit: PropTypes.func,
	onDismissSubmitError: PropTypes.func,
	submitErrorMsg: PropTypes.string,
	redirect: PropTypes.bool
};

export default JobApplyPresentation;