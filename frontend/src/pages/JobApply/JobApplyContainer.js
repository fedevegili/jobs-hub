import React from "react";
import JobApplyPresentation from './JobApplyPresentation';
import withJobs from "../../hocs/withJobs";
import PropTypes from "prop-types";
import JobAPI from "../../api/JobAPI";

const Presentation = withJobs(JobApplyPresentation);

class JobApplyContainer extends React.Component {
	constructor() {
		super();

		const me = this;

		me.state = {
			submitErrorMsg: undefined,
			redirect: false
		};

		me.submit = me.submit.bind(me);
		me.dismissSubmitErrors = me.dismissSubmitErrors.bind(me);
	}

	dismissSubmitErrors() {
		this.setState({
			submitErrorMsg: undefined
		});
	}

	submit(data) {
		const me = this;

		me.dismissSubmitErrors();

		return me.props.jobAPI.applyForJob(me.props.match.params.id, data)
			.then((response) => {
				if (!response.ok) {
					throw Error(response.statusText);
				}

				return response;
			})
			.then(() => {
				me.setState({
					redirect: true
				});
			})
			.catch((error) => {
				me.setState({
					submitErrorMsg: error.message
				})
			});
	}

	render() {
		const me = this, props = this.props;

		return (
			<Presentation
				jobId={props.match.params.id}
				jobAPI={props.jobAPI}
				onSubmit={me.submit}
				submitErrorMsg={me.state.submitErrorMsg}
				onDismissSubmitError={me.dismissSubmitErrors}
				redirect={me.state.redirect}
			/>
		);
	}
}

JobApplyContainer.propTypes = {
	jobAPI: PropTypes.instanceOf(JobAPI).isRequired,
	match: PropTypes.object.isRequired
};

export default JobApplyContainer;