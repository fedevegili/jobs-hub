import React from 'react';
import { mount } from 'enzyme';
import JobPresentation from '../JobPresentation';
import { MemoryRouter } from "react-router-dom";
import {jobA} from "../../../__mocks__/jobData";


it('should render some jobs', () => {
	const wrapper = mount(
		<MemoryRouter>
			<JobPresentation job={jobA} />
		</MemoryRouter>
	);

	expect(wrapper.find(JobPresentation)).toHaveLength(1);
});