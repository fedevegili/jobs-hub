import React from 'react';
import { mount } from 'enzyme';
import JobContainer from '../JobContainer';
import JobAPI from "../../../api/JobAPI";

it('should render', () => {
	const wrapper = mount(
		<JobContainer match={{params: {id: 'abc'}}} jobAPI={new JobAPI()} />
	);

	expect(wrapper.children().props().jobId).toEqual('abc');
});