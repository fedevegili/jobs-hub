import React from "react";
import JobPresentation from './JobPresentation';
import withJobs from "../../hocs/withJobs";

const Presentation = withJobs(JobPresentation);

const JobContainer = (props) => {
	return (
		<Presentation jobId={props.match.params.id} jobAPI={props.jobAPI} />
	);
};

export default JobContainer;