import React from "react";
import styles from './JobPresentation.module.scss';
import Button, {sizes, kinds} from '../../components/Button/Button';
import {ReactComponent as Location} from '../../components/svg/location.svg';
import {ReactComponent as SuitCase} from '../../components/svg/suitcase.svg';
import PropTypes from 'prop-types';
import UnStyledLink from "../../components/UnStyledLink/UnStyledLink";
import {ROUTE_JOB_APPLY_FN, ROUTE_JOB_LIST_FN} from "../routes";
import ButtonToolbar from "../../components/ButtonToolbar/ButtonToolbar";
import LineBreak from "../../components/LineBreak/LineBreak";

function getJobInfo(job) {
	return (
		<React.Fragment>
			<h2 className={styles.jobTitle}>{job.title}</h2>
			<span>
					<Location className={styles.locationIcon} />
					<a
						href={`https://maps.google.com/maps?q=${job.postal_code}&t=&z=13`}
						target="_blank"
						rel="noopener noreferrer"
					>{job.location}</a>
				</span>
			<br />
			<span>
					<SuitCase className={styles.locationIcon} />
					<span>{job.job_type}</span>
				</span>
			<div>
				<div dangerouslySetInnerHTML={{__html: job.description}} />
			</div>
		</React.Fragment>
	);
}

function JobPresentation(props) {
	const job = props.job;

	const backBtn = (
		<UnStyledLink to={ROUTE_JOB_LIST_FN()}>
			<Button>{"Back to job list"}</Button>
		</UnStyledLink>
	);

	const applyBtn = (
		<UnStyledLink to={ROUTE_JOB_APPLY_FN(job.id)}>
			<Button
				size={sizes.BUTTON_MEDIUM}
				kind={kinds.BUTTON_ATTENTION}
			>
				{"APPLY NOW"}
			</Button>
		</UnStyledLink>
	);

	return (
		<div className={styles.wrapper}>
			<ButtonToolbar
				buttons={[
					backBtn,
					applyBtn
				]}
			/>
			<LineBreak />
			{getJobInfo(job)}
			{applyBtn}
		</div>
	);
}

JobPresentation.propTypes = {
	job: PropTypes.object
};

export default JobPresentation;