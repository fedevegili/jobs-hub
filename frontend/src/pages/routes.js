export const ROUTE_JOB_LIST_FN = () => ("/");

export const ROUTE_JOB_INFO_FN = (id) => (`/jobs/${id}`);

export const ROUTE_JOB_APPLY_FN = (id) => (`/jobs/${id}/apply`);

export const ROUTE_JOB_FEEDBACK_FN = (id) => (`/jobs/${id}/feedback`);