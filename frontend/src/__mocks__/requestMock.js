export const successRequest = (data) => {
	return () => {
		return new Promise((resolve, reject) => {
			resolve({
				ok: true,
				json: () => (data)
			});
		});
	}
};

export const successPromiseFailedRequest = (text) => {
	return () => {
		return new Promise((resolve, reject) => {
			resolve({
				ok: false,
				statusText: text
			});
		});
	}
};

export const failedRequest = (data) => {
	return () => {
		return new Promise((resolve, reject) => {
			reject(data);
		});
	}
};