import React from 'react';
import ReactDOM from 'react-dom';
import UIApp from './pages/UIApp/UIApp';

import './style/styling';

ReactDOM.render(
	<UIApp />,
	document.getElementById('root')
);